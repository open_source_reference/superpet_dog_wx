import { Router, Common } from "../../utils/common.js";
import { Pet } from "../../model/pet.js";
import { RemoteDataService } from '../../utils/remoteDataService.js';
const util = require('../../utils/util.js')

Page({
  data: {
    date: "2017-01-01",
    pickEndDate: "2018-12-31",
    src: "/style/images/upload.png",
    neu: -1,
    sex: -1,
    pet: {}, //for save data
    petVali: {} // for validate
  },

  onLoad: function (options) {
    let that = this
    console.log("Return value")
    console.log(that.data.pet)
    console.log(that.data.petVali)
    let endDate = util.formatDate(new Date())
    console.log("endDate", endDate)
    if (options.pet){
      that.data.pet = JSON.parse(options.pet)
      that.data.petVali = JSON.parse(options.petVali)
      console.log(that.data.pet)
      console.log(that.data.petVali)
      that.setData({
        pet: that.data.pet,
        petVali: that.data.petVali,
        date: that.data.pet.birthday,
        src: that.data.pet.avatarTmp || "/style/images/upload.png",
        neu: that.data.pet.isNeuter,
        sex: that.data.pet.sex,
        pickEndDate: endDate
      })
    }else{//add and first coming
      that.data.pet.birthday = "2017-01-01"
      that.data.petVali.birthdayOk = true
      that.setData({
        pickEndDate: endDate
      })
    }
  },
  sexTap: function (e) {
    let val = e.currentTarget.dataset.sexval
    let that = this
    if (val == 1) {
      that.data.pet.sex = 1
      that.setData({
        sex: 1
      })
      that.data.petVali.sexOk = true
    } else if (val == 0) {
      that.data.pet.sex = 0
      that.setData({
        sex: 0
      })
      that.data.petVali.sexOk = true
    }
  },
  neuTap: function(e){
    let val = e.currentTarget.dataset.neuval
    let that = this
    if (val==1){
      that.data.pet.isNeuter = 1
      that.setData({
        neu: 1
      })
      that.data.petVali.neuOk = true
    }else if(val==0){
      that.data.pet.isNeuter = 0
      that.setData({
        neu: 0
      })
      that.data.petVali.neuOk = true
    }
  },
  bindDateChange: function (e) {
    this.data.pet.birthday = e.detail.value
    this.setData({
      date: e.detail.value
    })
    this.data.petVali.birthdayOk = true
  },
  getPetName: function(e){
    this.data.pet.petName = e.detail.value
    console.log("this.data.pet.petName====" + this.data.pet.petName)
    if (this.data.pet.petName){
      this.data.petVali.petNameOk = true
    }else{
      this.data.petVali.petNameOk = false
    }
    console.log("this.data.petVali.petNameOk=="+this.data.petVali.petNameOk)
  },
  getPetWeight: function (e) {
    this.data.pet.weight = e.detail.value
    console.log("this.data.pet.weight====" + this.data.pet.weight)
    if (this.data.pet.weight) {
      this.data.petVali.weightOk = true
    } else {
      this.data.petVali.weightOk = false
    }
  },
  getPetBreed: function(e){
    this.data.pet.breed = e.detail.value
    console.log("this.data.pet.breed====" + this.data.pet.breed)
    if (this.data.pet.breed) {
      this.data.petVali.breedOk = true
    } else {
      this.data.petVali.breedOk = false
    }
  },
  getPetFeature: function (e) {
    this.data.pet.feature = e.detail.value
    console.log("this.data.pet.feature====" + this.data.pet.feature)
    if (this.data.pet.feature) {
      this.data.petVali.featureOk = true
    } else {
      this.data.petVali.featureOk = false
    }
  },
  upload: function(){
    let that = this
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success(res) {
        let src = res.tempFilePaths[0]
        console.log(that.data.pet)
        console.log(that.data.petVali)
        let params = {
          src: src,
          pet: JSON.stringify(that.data.pet),
          petVali: JSON.stringify(that.data.petVali)
        }
        console.log("go value")
        console.log(params)
        Router.redirectTo("../upload/upload", params);
      }
    })
  },
  saveData: function(){
    let that = this;
    let petVali = that.data.petVali;
    if (!petVali.petNameOk){
      Common.myToast("请输入名字",2000);
      return;
    } else if (!petVali.sexOk){
      Common.myToast("请选择性别", 2000);
      return;
    } else if (!petVali.birthdayOk){
      Common.myToast("请输入生日", 2000);
      return;
    } else if (!petVali.weightOk){
      Common.myToast("请输入体重", 2000);
      return;
    } else if (!petVali.breedOk) {
      Common.myToast("请输入品种", 2000);
      return;
    } else if (!petVali.featureOk){
      Common.myToast("请输入描述", 2000);
      return;
    } else if (!petVali.neuOk){
      Common.myToast("请选择是否绝育", 2000);
      return;
    } else if (!petVali.avatarOk){
      Common.myToast("请上传头像", 2000);
      return;
    }
    wx.showNavigationBarLoading()
    RemoteDataService.savePetInfo(that.data.pet).then(result => {
      if (result && result.code == "000") {
        if (that.data.pet.id){
          let params = {
            petId: that.data.pet.id
          }
          Router.redirectTo("../dogdet/dogdet", params);
        }else{
          let params = {
            petNo: result.petNo,
            petAvatar: that.data.src,
            petName: that.data.pet.petName
          }
          Router.redirectTo("../dogaddcomp/dogaddcomp", params);
        }
      }
      wx.hideNavigationBarLoading()
    }).catch(err => {
      wx.hideNavigationBarLoading()
    })
  }
  
})